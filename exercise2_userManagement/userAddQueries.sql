CREATE DATABASE vtapp;

CREATE USER 'vtapp_user'@'localhost' IDENTIFIED BY 'vinsol';

GRANT ALL PRIVILEGES ON 'vtapp'.* TO 'vtapp_user'@'localhost';
