SELECT c.name, r.name FROM colors c
INNER JOIN mixtures m ON m.parent1_id = c.id OR m.parent2_id = c.id
INNER JOIN colors r ON m.mix_id = r.id
INNER JOIN colors cc ON m.parent2_id = cc.id OR m.parent1_id = cc.id
where cc.name = 'green' and c.name != 'green';

+--------+--------+
| name   | name   |
+--------+--------+
| green  | yellow |
| blue   | pink   |
| yellow | white  |
+--------+--------+



SELECT r.name FROM colors r, mixtures
WHERE mixtures.mix_id = r.id
AND mixtures.parent1_id != (SELECT @parent := id FROM colors WHERE name = 'red')
AND mixtures.parent2_id != @parent; 
+------+
| name |
+------+
| cyan |
+------+
1 row in set (0.00 sec)


SELECT c.name AS 'parent name', group_concat(n.name SEPARATOR ' & ') AS 'possible mixture'
FROM colors c
INNER JOIN mixtures m ON c.id = m.parent1_id OR c.id=m.parent2_id
INNER JOIN colors n ON m.mix_id = n.id
GROUP BY c.name
ORDER BY c.id;
+-------------+-----------------------+
| parent name | possible mixture      |
+-------------+-----------------------+
| red         | yellow & white & pink |
| green       | yellow & cyan         |
| blue        | cyan & pink           |
| yellow      | white                 |
+-------------+-----------------------+
4 rows in set (0.01 sec)





SELECT concat(m.parent1_id, ' & ', m.parent2_id) AS 'parent colors', 
  m.mix_id, m.mix_density, c.density 'original density'
FROM mixtures m
INNER JOIN colors c ON c.density < m.mix_density AND m.mix_id = c.id;

+---------------+--------+-------------+------------------+
| parent colors | mix_id | mix_density | original density |
+---------------+--------+-------------+------------------+
| 10 & 11       |     13 |        0.60 |             0.20 |
| 10 & 12       |     14 |        0.50 |             0.30 |
| 11 & 12       |     15 |        0.75 |             0.40 |
+---------------+--------+-------------+------------------+
3 rows in set (0.00 sec)



SELECT ROUND((IFNULL(SUM(m.parent1_id * m.parent1_perc),0)+IFNULL(SUM(mm.parent2_id*mm.parent2_perc),0))/1000,1) amount
FROM mixtures m
INNER JOIN colors ON m.parent1_id = colors.id AND colors.name = 'green'
LEFT JOIN mixtures mm ON mm.parent2_id = colors.id AND colors.name = 'green';
+--------+
| amount |
+--------+
|    1.6 |
+--------+
1 row in set (0.00 sec)

