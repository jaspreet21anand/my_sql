SELECT shape, color, ROUND(CASE shape WHEN 'triangle' THEN POW(3, 0.5)*POW(side1, 2)/4
    ->                    WHEN 'circle' THEN 3.14*POW(side1, 2)
    ->                    WHEN 'rectangle' THEN side1*side2
    ->                    WHEN 'square' THEN POW(side1, 2) END, 3) AS AREA FROM blocks;
+-----------+--------+---------+
| shape     | color  | AREA    |
+-----------+--------+---------+
| triangle  | red    |  10.825 |
| square    | red    |  25.000 |
| rectangle | red    |   8.000 |
| circle    | red    |  78.500 |
| circle    | blue   |  12.560 |
| square    | blue   |  49.000 |
| square    | yellow |  64.000 |
| triangle  | yellow |   1.732 |
| triangle  | blue   |   3.897 |
| circle    | green  | 314.000 |
| square    | green  |   1.000 |
| square    | cyan   |  16.000 |
| rectangle | cyan   |  56.000 |
| rectangle | yellow |  10.000 |
| rectangle | white  |  20.000 |
| circle    | white  |  50.240 |
+-----------+--------+---------+
16 rows in set (0.00 INNER)



SELECT shape, SUM(ROUND(CASE shape WHEN 'triangle' THEN POW(3, 0.5)*POW(side1, 2)/4
    ->                    WHEN 'circle' THEN 3.14*POW(side1, 2)
    ->                    WHEN 'rectangle' THEN side1*side2
    ->                    WHEN 'square' THEN POW(side1, 2) END, 3)) AS AREA FROM blocks
    -> GROUP BY shape;
+-----------+---------+
| shape     | AREA    |
+-----------+---------+
| circle    | 455.300 |
| rectangle |  94.000 |
| square    | 155.000 |
| triangle  |  16.454 |
+-----------+---------+
4 rows in set (0.00 INNER)



SELECT shape, color, bq.qty*ROUND(CASE shape WHEN 'triangle' THEN POW(3, 0.5)*POW(side1,2)/4
    ->                    WHEN 'circle' THEN 3.14*POW(side1,2)
    ->                    WHEN 'rectangle' THEN side1*side2
    ->                    WHEN 'square' THEN POW(side1,2) END, 3) AS AREA FROM blocks
    -> INNER JOIN block_qty bq ON bq.block_id = blocks.id;
+-----------+--------+----------+
| shape     | color  | AREA     |
+-----------+--------+----------+
| triangle  | red    |   75.775 |
| square    | red    |  125.000 |
| rectangle | red    |   24.000 |
| circle    | red    |  157.000 |
| circle    | blue   |    0.000 |
| square    | blue   |  245.000 |
| square    | yellow |  320.000 |
| triangle  | yellow |   13.856 |
| triangle  | blue   |   15.588 |
| circle    | green  | 2512.000 |
| square    | green  |    8.000 |
| square    | cyan   |   96.000 |
| rectangle | cyan   |  336.000 |
| rectangle | yellow |   10.000 |
| rectangle | white  |  140.000 |
+-----------+--------+----------+
15 rows in set (0.00 INNER)



SELECT shape, color, SUM(bq.qty*ROUND(CASE shape WHEN 'triangle' THEN POW(3, 0.5)*POW(side1, 2)/4
    ->                    WHEN 'circle' THEN 3.14*POW(side1, 2)
    ->                    WHEN 'rectangle' THEN side1*side2
    ->                    WHEN 'square' THEN POW(side1,2) END, 3)) AS AREA FROM blocks
    -> INNER JOIN block_qty bq ON bq.block_id = blocks.id
    -> GROUP BY color HAVING AREA > 15*20;
+----------+--------+----------+
| shape    | color  | AREA     |
+----------+--------+----------+
| square   | cyan   |  432.000 |
| circle   | green  | 2520.000 |
| triangle | red    |  381.775 |
| square   | yellow |  343.856 |
+----------+--------+----------+
4 rows in set (0.00 INNER)
