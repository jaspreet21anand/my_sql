--queries for sandwich db

SELECT location FROM sandwiches
  WHERE filling IN (SELECT filling FROM tastes WHERE name = 'jones');

SELECT location FROM sandwiches
  INNER JOIN tastes ON tastes.filling = sandwiches.filling AND tastes.name = 'jones';

SELECT location, COUNT(DISTINCT tastes.name) AS no_of_persons
FROM sandwiches
  INNER JOIN tastes ON tastes.filling = sandwiches.filling
GROUP BY location;
