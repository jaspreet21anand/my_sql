-- Multibranch library


SELECT title FROM titles WHERE publisher = 'mcmillan';

SELECT branch FROM holdings
  WHERE title IN (
    SELECT title FROM titles WHERE author = 'ann brown');

SELECT branch FROM holdings
  INNER JOIN titles ON titles.title = holdings.title AND titles.author = 'ann brown';

SELECT branch, SUM(copies) AS number_of_copies FROM holdings GROUP BY branch;
