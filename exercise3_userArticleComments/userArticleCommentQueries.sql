SET @userName = 'sid';

SELECT articles.id, content FROM articles
INNER JOIN users ON articles.user_id = users.id AND users.name = @user;

+----+-----------------------------------------------------------------+
| id | content                                                         |
+----+-----------------------------------------------------------------+
|  5 | sd sdkjsd fslkj sdflkjsfdl fsgsffsdkl; dfsk;a; dsfiui  dfsdfgkj |
+----+-----------------------------------------------------------------+

--For all the articles being selected above, select all the articles and also the comments 
--associated with those articles in a single query
SELECT articles.id, articles.content, GROUP_CONCAT(comments.content) AS Comments
FROM articles
  INNER JOIN users ON articles.user_id = users.id AND users.name = @user
    INNER JOIN comments ON articles.id = comments.article_id GROUP BY comments.article_id;

+----+-----------------------------------------------------------------+------------------------------------------------------+
| id | content                                                         | Comments                                             |
+----+-----------------------------------------------------------------+------------------------------------------------------+
|  5 | sd sdkjsd fslkj sdflkjsfdl fsgsffsdkl; dfsk;a; dsfiui  dfsdfgkj | yeah cool work done haha,kjk  jjj yehu done the work |
+----+-----------------------------------------------------------------+------------------------------------------------------+


--For all the articles being selected above, select all the articles and also the comments 
--associated with those articles in a single query
--using subquery
SELECT articles.id, articles.content, group_concat(comments.content) AS comments
FROM articles, (SELECT comments.content FROM comments
  WHERE comments.article_id in (SELECT articles.id FROM articles
    WHERE user_id = (SELECT users.id FROM users WHERE users.name = 'sid'))) AS comments
      WHERE articles.user_id = (SELECT users.id FROM users WHERE users.name = 'sid');

+----+-----------------------------------------------------------------+------------------------------------------------------+
| id | content                                                         | comments                                             |
+----+-----------------------------------------------------------------+------------------------------------------------------+
|  5 | sd sdkjsd fslkj sdflkjsfdl fsgsffsdkl; dfsk;a; dsfiui  dfsdfgkj | yeah cool work done haha,kjk  jjj yehu done the work |
+----+-----------------------------------------------------------------+------------------------------------------------------+
--articles which doesn't have any comments.

SELECT articles.id AS article, articles.content FROM articles
  LEFT JOIN comments ON articles.id = comments.article_id
WHERE comments.id is NULL;
+---------+--------------------------------------------+
| article | content                                    |
+---------+--------------------------------------------+
|       4 | sd sdkjsd fslkj sdflkjsfdl fsgsf  dfsdfgkj |
+---------+--------------------------------------------+

--articles which doesn't have any comments. using subquery

SELECT articles.id AS article, articles.content FROM articles
WHERE articles.id NOT IN(
  SELECT comments.article_id
  FROM comments);

+---------+--------------------------------------------+
| article | content                                    |
+---------+--------------------------------------------+
|       4 | sd sdkjsd fslkj sdflkjsfdl fsgsf  dfsdfgkj |
+---------+--------------------------------------------+

--articles with max number of comments
SELECT a.content, COUNT(c.content) AS ncomments
FROM articles a
INNER JOIN comments c ON a.id = c.article_id
GROUP BY c.article_id HAVING ncomments = (
  SELECT COUNT(content) AS no_of_comments
  FROM comments
  GROUP BY article_id 
  ORDER BY no_of_comments DESC LIMIT 1
);

+-----------------------------------------------------------------+-----------+
| content                                                         | ncomments |
+-----------------------------------------------------------------+-----------+
| sd sdkjsd sdksd dsfklj                                          |         2 |
| sd df                                                           |         2 |
| sd sdkjsd fslkj sdflkjsfdl fsgsffsdkl; dfsk;a; dsfiui  dfsdfgkj |         2 |
+-----------------------------------------------------------------+-----------+
3 rows in set (0.01 sec)


--articles having 1 comment by 1 user

SELECT articles.id article
FROM articles
LEFT JOIN comments ON articles.id = comments.article_id
GROUP BY comments.article_id HAVING COUNT(comments.user_id) = COUNT(DISTINCT comments.user_id);

+---------+
| article |
+---------+
|       4 |
|       2 |
+---------+
2 rows in set (0.04 sec)

