--most tagges images
SELECT image_id, count(*) AS count FROM tags GROUP BY image_id HAVING count = (
  SELECT count(*) AS count FROM tags GROUP BY image_id ORDER BY count DESC LIMIT 1);

+----------+-------+
| image_id | count |
+----------+-------+
|        1 |     3 |
|        3 |     3 |
+----------+-------+

--images belonging to friends of particular user
SELECT * FROM images 
INNER JOIN friends ON friends.friend = images.image_user AND friends.user_id = 1;
+----+------------+---------+--------+
| id | image_user | user_id | friend |
+----+------------+---------+--------+
|  1 |          2 |       1 |      2 |
|  5 |          2 |       1 |      2 |
|  3 |          4 |       1 |      4 |
|  4 |          3 |       1 |      3 |
+----+------------+---------+--------+

--tagged in all pics of his friends
SELECT friends.friend
FROM friends
INNER JOIN images
ON friends.friend = images.image_user
AND friends.user_id = 2
INNER JOIN tags
ON tags.image_id = images.id
GROUP BY friends.friend
HAVING SUM(tags.tagged = friends.user) = COUNT(DISTINCT tags.image_id);
+--------+
| friend |
+--------+
|      4 |
+--------+



SELECT friends.friend, COUNT(*) no_of_tags
FROM friends
INNER JOIN images
ON friends.friend = images.image_user AND friends.user_id = 1
INNER JOIN tags ON tags.image_id = images.id and tags.tagged = friends.user_id
GROUP BY friends.friend
HAVING no_of_tags = (
  SELECT COUNT(*)
  FROM images
  INNER JOIN friends ON friends.friend = images.image_user AND friends.user_id = 1
  INNER JOIN tags ON tags.image_id = images.id and tags.tagged = friends.user_id
  GROUP BY friend
  ORDER BY COUNT(*) DESC LIMIT 1
);
+--------+------------+
| friend | no_of_tags |
+--------+------------+
|      2 |          2 |
+--------+------------+
1 row in set (0.00 sec)

