--deposit 1000 in user 1
START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT @accNo := SELECT FROM users WHERE name = 'jaspreet';
+----------------------+
| @accNo := account_no |
+----------------------+
|                  123 |
+----------------------+
1 row in set (0.00 sec)

mysql> UPDATE accounts SET balance = balance + 1000 WHERE account_no = @accNo;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM accounts;
+----+------------+---------+
| id | account_no | balance |
+----+------------+---------+
|  1 |        123 |    6000 |
|  2 |        124 |   10000 |
|  3 |        125 |    2000 |
+----+------------+---------+
3 rows in set (0.00 sec)

mysql> COMMIT;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT * FROM accounts;
+----+------------+---------+
| id | account_no | balance |
+----+------------+---------+
|  1 |        123 |    6000 |
|  2 |        124 |   10000 |
|  3 |        125 |    2000 |
+----+------------+---------+
3 rows in set (0.00 sec)







--user 1 withdrawing 500
START TRANSACTION ;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT @accNo := account_no FROM users WHERE user = 'jaspreet';
ERROR 1054 (42S22): Unknown column 'user' in 'where clause'
mysql> SELECT @accNo := account_no FROM users WHERE name = 'jaspreet';
+----------------------+
| @accNo := account_no |
+----------------------+
|                  123 |
+----------------------+
1 row in set (0.00 sec)

mysql> UPDATE accounts SET balance = balance - 500 WHERE account_no = @accNo;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM accounts;
+----+------------+---------+
| id | account_no | balance |
+----+------------+---------+
|  1 |        123 |    5500 |
|  2 |        124 |   10000 |
|  3 |        125 |    2000 |
+----+------------+---------+
3 rows in set (0.00 sec)

mysql> COMMIT;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT * FROM accounts;
+----+------------+---------+
| id | account_no | balance |
+----+------------+---------+
|  1 |        123 |    5500 |
|  2 |        124 |   10000 |
|  3 |        125 |    2000 |
+----+------------+---------+
3 rows in set (0.00 sec)



--user1 transfer 200 to user2
START TRANSACTION;
Query OK, 0 rows affected (0.00 sec)

mysql> SET @transferAmount = 200;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT @accNo := account_no FROM users WHERE name = 'jaspreet';
+----------------------+
| @accNo := account_no |
+----------------------+
|                  123 |
+----------------------+
1 row in set (0.00 sec)

mysql> UPDATE accounts SET balance = balance - @transferAmount WHERE account_no = @accNo;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> UPDATE accounts SET balance = balance - @transferAmount WHERE account_no = @accNo;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT @accNo := account_no FROM users WHERE name = 'sid';
+----------------------+
| @accNo := account_no |
+----------------------+
|                  125 |
+----------------------+
1 row in set (0.00 sec)

mysql> UPDATE accounts SET balance  = balance + @transferAmount WHERE account_no = @accNo;
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> SELECT * FROM accounts;
+----+------------+---------+
| id | account_no | balance |
+----+------------+---------+
|  1 |        123 |    5100 |
|  2 |        124 |   10000 |
|  3 |        125 |    2200 |
+----+------------+---------+
3 rows in set (0.00 sec)

mysql> COMMIT;
Query OK, 0 rows affected (0.00 sec)

mysql> SELECT * FROM accounts;
+----+------------+---------+
| id | account_no | balance |
+----+------------+---------+
|  1 |        123 |    5100 |
|  2 |        124 |   10000 |
|  3 |        125 |    2200 |
+----+------------+---------+
3 rows in set (0.00 sec)