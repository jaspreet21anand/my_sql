CREATE TABLE testing_table(
name VARCHAR(10),
contact_name VARCHAR(10),
roll_no VARCHAR(10));

ALTER TABLE testing_table DROP name;

ALTER TABLE testing_table CHANGE contact_name username VARCHAR(10);

ALTER TABLE testing_table ADD (first_name VARCHAR(10), last_name VARCHAR(10));

ALTER TABLE testing_table MODIFY roll_no INT;
