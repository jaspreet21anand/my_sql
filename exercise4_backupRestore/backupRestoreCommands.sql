--in command line outside mysql
mysqldump -u root -p joins > ~/Documents/Jaspreet/Exercises/My_SQL/exercise4_backupRestore/dbdump.sql

--create database in mysql as "restore"
create database restore;

--in command line outside mysql
mysql -u root -p restore < ~/Documents/Jaspreet/Exercises/My_SQL/exercise4_backupRestore/dbdump.sql