SELECT e.name, c.employee_id, SUM(c.commission_amount) AS total
FROM commissions as c
INNER JOIN employees AS e ON e.id = c.employee_id
GROUP BY employee_id ORDER BY total DESC
LIMIT 1;

+-------------+-------------+-------+
| name        | employee_id | total |
+-------------+-------------+-------+
| chris gayle |           1 |  9000 |
+-------------+-------------+-------+
1 row in set (0.00 sec)



SELECT * FROM employees
ORDER BY salary DESC LIMIT 3,1;
+----+--------------+--------+---------------+
| id | name         | salary | department_id |
+----+--------------+--------+---------------+
|  3 | rahul dravid | 700000 |             1 |
+----+--------------+--------+---------------+
1 row in set (0.00 sec)


SELECT departments.name, department_id, sum(commission_amount) total_commission
FROM commissions
INNER JOIN employees ON commissions.employee_id = employees.id
INNER JOIN departments ON department_id = departments.id
GROUP BY department_id
ORDER BY total_commission DESC LIMIT 1;
+---------------+------------------+
| department_id | total_commission |
+---------------+------------------+
|             1 |            13000 |
+---------------+------------------+
1 row in set (0.00 sec)




SELECT GROUP_CONCAT(e.name), c.commission_amount AS commission
FROM commissions c
INNER JOIN employees e
ON e.id = c.employee_id
GROUP BY commission HAVING commission > 3000; 
+--------------------------+------------+
| group_concat(e.name)     | commission |
+--------------------------+------------+
| chris gayle,rahul dravid |       4000 |
| chris gayle,wasim akram  |       5000 |
+--------------------------+------------+
2 rows in set (0.00 sec)

